#![allow(unused, dead_code)]
mod inputs;
use rand::Rng;
use std::collections::{BTreeMap, BTreeSet, HashMap, HashSet, VecDeque};
use std::fmt::{self, Display};
use std::hash::{Hash, Hasher};

struct Solution();

#[derive(Debug, Hash, Clone, Eq, PartialEq)]
enum Value {
    Num(u8),
    King,
    Queen,
    Jack,
}

#[derive(Debug, Hash, Clone, Eq, PartialEq)]
enum Suite {
    Spades,
    Clubs,
    Hearts,
    Diamonds,
}

impl Display for Suite {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Suite::Spades => "Spades",
                Suite::Clubs => "Clubs",
                Suite::Hearts => "Hearts",
                Suite::Diamonds => "Diamonds",
            }
        )
    }
}

#[derive(Debug, Hash, Clone, Eq, PartialEq)]
struct Card {
    val: Value,
    suite: Suite,
}

impl Display for Value {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Value::Num(x) => {
                    match x {
                        0 => "Ace".to_owned(),
                        1..=9 => (x + 1).to_string(),
                        _ => unreachable!(),
                    }
                }
                Value::Jack => "Jack".to_owned(),
                Value::Queen => "Queen".to_owned(),
                Value::King => "King".to_owned(),
            }
        )
    }
}

impl From<u8> for Card {
    fn from(val: u8) -> Self {
        Self {
            val: match val % 13 {
                0..=9 => Value::Num(val % 13),
                10 => Value::Jack,
                11 => Value::Queen,
                12 => Value::King,
                _ => unreachable!(),
            },
            suite: match val % 52 {
                0..=12 => Suite::Spades,
                13..=25 => Suite::Clubs,
                26..=38 => Suite::Hearts,
                39..=51 => Suite::Diamonds,
                _ => unreachable!(),
            },
        }
    }
}

impl Display for Card {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} of {}", self.val, self.suite)
    }
}

impl TryFrom<&Card> for u8 {
    type Error = String;
    fn try_from(card: &Card) -> Result<u8, Self::Error> {
        let mut res = 13
            * match card.suite {
                Suite::Spades => 0,
                Suite::Clubs => 1,
                Suite::Hearts => 2,
                Suite::Diamonds => 3,
            };
        match card.val {
            Value::Num(x) => match x {
                0..=9 => Ok(x + res),
                x => Err(format!("invalid card value: {}", x)),
            },
            Value::Jack => Ok(10 + res),
            Value::Queen => Ok(11 + res),
            Value::King => Ok(12 + res),
        }
    }
}

fn gen_deck() -> Vec<Card> {
    (0..52).map(|i| Card::from(i)).collect()
}

impl Solution {
    // Paste fn stub here...
    fn shuffle_cards() -> Vec<Card> {
        let mut deck = gen_deck();
        let mut rnd = rand::thread_rng();
        (0..52)
            .rev()
            .map(|r| rnd.gen_range(0..=r))
            .map(|n| deck.swap_remove(n))
            .collect()
    }
}

#[cfg(test)]
mod tests {
    use plotters::prelude::*;
    use statistical::{standard_deviation, mean};

    use crate::*;
    #[test]
    fn test_simple_1() {
        use std::collections::HashSet;
        let actual_result = Solution::shuffle_cards();
        assert_eq!(HashSet::<Card>::from_iter(actual_result.clone()).len(), 52);
    }

    use futures::future;
    #[tokio::test(flavor = "multi_thread")]
    async fn test_simple_2() {
        let mut pos_map = gen_deck()
            .into_iter()
            .map(|c| (c, vec![0; 52]))
            .collect::<HashMap<_, _>>();
        let n = 50000;
        let res = future::try_join_all((0..n).map(|_| tokio::spawn(async {Solution::shuffle_cards()}))).await;
        assert!(res.is_ok());

        res.unwrap().into_iter().for_each(|actual_result| {
                actual_result.iter().enumerate().for_each(|(i, c)| {pos_map.entry(c.clone()).and_modify(|pos| pos[i] += 1);});
        });
        let out_file_name = "histogram_shuffle_cards.png";
        plot_2d(out_file_name, &pos_map, n);
        let data = &pos_map.iter().map(|(k, v)| v.iter()).flatten().map(|x| *x as f64).collect::<Vec<_>>()[..];
        println!("Mean: {}, Std dev: {}", mean(data), standard_deviation(data, None)/n as f64);
    }

    fn plot_2d(
        out_file_name: &str,
        data: &HashMap<Card, Vec<i32>>,
        n: i32,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let root = BitMapBackend::new(out_file_name, (1024, 1024)).into_drawing_area();

        root.fill(&WHITE)?;

        let mut chart = ChartBuilder::on(&root)
            .caption("Shuffle Cards Example", ("sans-serif", 80))
            .margin(5)
            .top_x_label_area_size(40)
            .y_label_area_size(40)
            .build_cartesian_2d(0i32..104i32, 0i32..104i32)?;

        chart
            .configure_mesh()
            .x_labels(52)
            .y_labels(52)
            .x_label_offset(25)
            .y_label_offset(25)
            .disable_x_mesh()
            .disable_y_mesh()
            .label_style(("sans-serif", 20))
            .draw()?;

        chart.draw_series(
            data.iter()
                .enumerate()
                .map(|(y, (c, vi))| {
                    vi.iter()
                        .enumerate()
                        .map(move |(x, v)| (x as i32, y as i32, c, v))
                })
                .flatten()
                .map(|(x, y, c, v)| {
                    // &(EmptyElement::at((x, y)) + Text::new(format!("{}: {}/{}", c, v, n), [1, 1], ("sans-serif", 12)) +
                    Rectangle::new(
                        [(2 * x, 2 * y), (2 * x + 2, 2 * y + 2)],
                        RGBColor(26 * (*v as f64 * 256f64 / n as f64) as u8, 0, 0).filled(),
                    )
                    // )
                }),
        )?;

        // To avoid the IO failure being ignored silently, we manually call the present function
        root.present().expect("Unable to write result to file, please make sure 'plotters-doc-data' dir exists under current dir");
        println!("Result has been saved to {}", out_file_name);

        Ok(())
    }

}
