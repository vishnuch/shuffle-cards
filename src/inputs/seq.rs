use rand::Rng;

  pub fn bin_string(n: usize) -> String {
    let mut rnd = rand::thread_rng();
    (0..n).map(|_| ('0' as u8 + rnd.gen_range(0..2u8)) as char).collect()
  }

  pub fn range_string(n: usize, l: u8, r: u8) -> String {
    let mut rnd = rand::thread_rng();
    (0..n).map(|_| ('0' as u8 + rnd.gen_range(l..r)) as char).collect()
  }

  pub fn range_vec(n: usize, l: i32, r: i32) -> Vec<i32> {
    let mut rnd = rand::thread_rng();
    (0..n).map(|_| rnd.gen_range(l..r)).collect()
  }

  pub fn range_2dvec(m: usize, n: usize, l: i32, r: i32) -> Vec<Vec<i32>> {
    let mut rnd = rand::thread_rng();
    (0..m).map(|_| (0..n).map(|_| rnd.gen_range(l..r)).collect()).collect()
  }
    
